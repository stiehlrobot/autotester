
from pathlib import Path

BASEDIR = Path('.')
POSITION_BASEPATH = BASEDIR.joinpath('positions')
RESULTS_BASEPATH = BASEDIR.joinpath('backtest_results')
VOICEUTILS_BASEPATH = BASEDIR.joinpath('voiceutils')
