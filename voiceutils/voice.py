from gtts import gTTS
from io import BytesIO
from playsound import playsound
from settings import VOICEUTILS_BASEPATH


class Voice():
    
    def __init__(self):
        self.name = 'voice'
        
    def generate_and_speak(self, msg):        
        
        tts = gTTS(msg, lang='en', tld='com.au')
        save_name = '{}.mp3'.format(msg)
        
        s_path = VOICEUTILS_BASEPATH.joinpath(save_name)
        tts.save(s_path)
        print(s_path)
        playsound("{}".format(s_path))
