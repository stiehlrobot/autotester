import gspread
from oauth2client.service_account import ServiceAccountCredentials

class SheetManipulator():
    
    def __init__(self):
        
        self._client = None
        self._sheet = None
        self._worksheets = []
        self._authenticate()
    
    def _authenticate(self):
        scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
        creds = ServiceAccountCredentials.from_json_keyfile_name('creds.json', scope)
        self._client = gspread.authorize(creds)
    
    def connect_to_spreadsheet(self, ss_name):
        self._sheet = self._client.open(ss_name)
        
    def add_worksheet(self, wks_name):
        if self._sheet:
            self._sheet.add_worksheet(wks_name, rows="100", cols="20")
    
    def update_wks(self, wks_name, d_range, cell_data):
        wks = self._sheet.worksheet(wks_name)
        wks.update(d_range, cell_data)
            
    def next_empty_row(self, wks_name):      
        wks = self._sheet.worksheet(wks_name)  
        str_list = list(filter(None, wks.col_values(1)))
        return str(len(str_list)+1)
      

def main():
    mani = SheetManipulator()
    mani.connect_to_spreadsheet("KillerWhale Free version results")
    #mani.add_worksheet('YOLOSHEET') 
    mani.update_wks('YOLOSHEET', 'A1:H1',[[1, 2,3,4,5,6,7,8]])
    
    
    # Update a range of cells using the top left corner address
    #wks.update('A1', [[1, 2], [3, 4]])

    # Or update a single cell
    #wks.update('A1', "YOLO")

    # Format the header
    #wks.format('A1:B1', {'textFormat': {'bold': True}})
    
if __name__ == '__main__':
    main()