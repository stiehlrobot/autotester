import csv
import json

class Recorder:
    
    def __init__(self, save_path):
        self.save_path = save_path
        self.buttons_and_locations = {}
    
    def display_recorded(self):
        for key,val in self.buttons_and_locations.items():
            print(key, val['tab'], val['position'])
    
    def start_recording(self):
        while True:
            usr_input = input("input command: ").strip()
            if usr_input == 'r':
                tab_name = input('Provide tab name: ')
                while tab_name == '':
                    tab_name = input('Provide tab name: ')
                item_name = input('Provide item name: ')
                while item_name == '':
                    item_name = input('Provide item name: ')
                print("Recording location for '{}' on tab {}".format(item_name, tab_name))
                usr_confirmation = input("Move cursor to desired location.. press 's' when ready to save x,y co-ordinates.")
                if usr_confirmation == 's':
                    x, y = pag.position()
                    self.buttons_and_locations[item_name] = {'tab':tab_name, 'x_pos': x, 'y_pos': y}
                    print('Saved {} with x: {}, y: {}'.format(item_name, x, y))
                
            if usr_input == 'd':
                print("")
                self.display_recorded()
            
            if usr_input == 'q':
                self.save_records()
                print("exit")
                sys.exit(0)
                
            else:
                print("unrecognized command")
                
    def save_records(self):        
        dict_to_json(self.buttons_and_locations, self.save_path)

def dict_to_json(data, fname):
    #json_string = json.dumps(self.buttons_and_locations)    
    with open(fname, 'w') as f:
        json.dump(data, f)
        

if __name__ == '__main__':
    rec = Recorder('positions_map.json')
    rec.start_recording()