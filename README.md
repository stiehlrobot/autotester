# autotester

Workaround for automating cryptohopper backtesting as cryptohopper API didn't provide backtesting options. Used for optimizing cryptohopper trading bots. 
Uses pyautogui to run tests with user input parameters. Saves results in google spreadsheets.

# environment

Install dependencies from **dependencies.txt**

# run

Once venv activated, run `python autotester.py`
