import time
import sys
import re
import csv
import json
import os
import settings
from datetime import datetime
from random import uniform
import pyperclip
import pyautogui as pag
from PIL import ImageGrab
from functools import partial
from gsheetutils.sheetmanipulator import SheetManipulator
from voiceutils.voice import Voice
from autotester_config import RunConfig
speaker = None


def dict_to_csv(posdata_dict):   
    
    if bool(posdata_dict):
        
        with open('posmap.csv', 'a', newline='') as csvfile:
            fieldnames = ['tab', 'item', 'x', 'y']
            writer = csv.writer(csvfile)

            writer.writerow(fieldnames)
            for key,val in posdata_dict.items():
                
                
                print("key:", key)
                print("x pos:", posdata_dict[key]['x_pos'])
                print("y pos:", posdata_dict[key]['y_pos'])
                print("pos:", posdata_dict[key]['tab'])
                                
                writer.writerow([posdata_dict[key]['tab'], key, posdata_dict[key]['x_pos'], posdata_dict[key]['y_pos']])

def enable_multi_screen_grabs():    
    ImageGrab.grab = partial(ImageGrab.grab, all_screens=True)

def load_json():
    with open('positions\posmap.json') as json_file:
        data = json.load(json_file)
    return data

def rand_timeout():
    return uniform(1.4, 1.8)

def rand_move_timeout():
    return uniform(0.7, 1.2)

def sleep_with_print(seconds):
    t_start = time.time()
    while time.time() < t_start + seconds:
        if int(time.time()) % 10 == 0:
            print('Elapsed time: {} - Time left: {}'.format(time.time()-t_start, seconds-(time.time()-t_start)))
            time.sleep(1)
            

def get_backtest_time_delta_days(input_datestring):
    """
    Example datestring format
    '05/1/2021 12:00 AM - 05/2/2021 11:59 PM'
    """
    
    section_1 = input_datestring.split('-')[0]
    
    st_day = section_1.split('/')[0]
    #print(st_day)
    st_month = section_1.split('/')[1]
    #print(st_month)
    st_year = section_1.split('/')[2].split(' ')[0]
    #print(st_year)
    
    st_hr = section_1.split('/')[2].split(' ')[1].split(':')[0]
    #print(st_hr)
    
    start = datetime(int(st_year), int(st_day), int(st_month), int(st_hr), 0, 0)
    
    section_2 = input_datestring.split('-')[1]
    
    end_day = section_2.split('/')[0]
    #print(end_day)
    end_month = section_2.split('/')[1]
    #print(end_month)
    end_year = section_2.split('/')[2].split(' ')[0]
    #print(end_year)
    
    end_hr = section_2.split('/')[2].split(' ')[1].split(':')[0]
    
    #print(end_hr)
    end = datetime(int(end_year), int(end_day),int(end_month), int(end_hr), 0, 0)
    
    dur = end - start
    
    #return interval in days
    return (dur.total_seconds() / 3600 / 24)

def speak_and_print(text):
    global speaker
    speaker.generate_and_speak(text)


def get_x_and_y_position(d, key):
    x = d[key]['x_pos']
    y = d[key]['y_pos']
    return x, y

"""
MAIN RUNNER SCRIPT
"""

def run_backtest(currency_dropdown_item_nr, strategy_name, date_range, take_profit_limit_percent, screen_num=2):
    
    
    '''
    prep vars if needed
    '''
    print_start(strategy_name, date_range, take_profit_limit_percent)
    
    if screen_num == 1:
        x_offset = -1920
    else:
        x_offset = 0
        
    y_offset = 20
        
    currency_key = 'currency_dropdown_{}'.format(currency_dropdown_item_nr)
    backtest_delay = (get_backtest_time_delta_days(date_range) / 7) * 150
    print('backtest delay: {}'.format(backtest_delay))
    
    data = load_json()
    
    #select hopper    
    x, y = get_x_and_y_position(data, 'hopper_select')
    print("x offset {}".format(x_offset))
    pag.moveTo((x + x_offset), (y + y_offset), duration=rand_move_timeout())
    pag.click()
    
    time.sleep(rand_timeout())
       
    x, y = get_x_and_y_position(data, 'hopper_one')
    pag.moveTo((x + x_offset), (y + y_offset), duration=rand_move_timeout())
    pag.click()    
    time.sleep(rand_timeout())
    
    #select backtesting tab    
    x, y = get_x_and_y_position(data, 'backtesting')
    pag.moveTo((x + x_offset), (y + y_offset), duration=rand_move_timeout())
    pag.click()    
    time.sleep(rand_timeout())
    
    """
    COIN SELECT
    """ 
    print("Selecting coin...")
    
    #activate currency dropdown, select currency 1
    x, y = get_x_and_y_position(data, 'currency_dropdown_select')
    pag.moveTo((x + x_offset), (y + y_offset), duration=rand_move_timeout())
    pag.click()    
    time.sleep(rand_timeout())

    x, y = get_x_and_y_position(data, currency_key)
    pag.moveTo((x + x_offset), (y + y_offset), duration=rand_move_timeout())
    pag.click()    
    time.sleep(rand_timeout())
        
    """
    STRATEGY SELECT
    """ 
    print("Selecting strategy...")
    
    #activate strategy dropdown    
    x, y = get_x_and_y_position(data, 'strategy_dropdown_select')
    pag.moveTo((x + x_offset), (y + y_offset), duration=rand_timeout())
    pag.click()    
    
    #type search keywords into field, type enter to select
    pag.typewrite(strategy_name, interval=0.19)
    time.sleep(rand_timeout())
    pag.press('enter')    
    time.sleep(rand_timeout())
    
    """
    TAKE PROFIT SELECT
    """ 
    
    print("Take profit select.. ")
        
    #move to take profit field, input take profit target value
    x, y = get_x_and_y_position(data, 'take_profit_input_field')
    pag.moveTo((x + x_offset), (y + y_offset), duration=rand_move_timeout())
    pag.tripleClick()    
    time.sleep(rand_timeout())
    
    pag.typewrite(str(take_profit_limit_percent), interval=0.33)    
    time.sleep(rand_move_timeout())
    
    """
    TIME PERIOD SELECT
    """ 
    print("Backtest period select.. ")
    
    #select period slider, select period date range input field
    x, y = get_x_and_y_position(data, 'select_period_slider')
    pag.moveTo((x + x_offset), (y + y_offset), duration=rand_move_timeout())
    pag.click()   
    
    x, y = get_x_and_y_position(data, 'select_period_date_range_input_field')
    pag.moveTo((x + x_offset), (y + y_offset), duration=rand_move_timeout())
    pag.tripleClick()
    
    #enter date in format 04/18/2021 12:00 AM - 04/21/2021 11:59 PM, finally click apply date
    pag.typewrite(date_range, interval=0.17)
    
    x, y = get_x_and_y_position(data, 'select_period_date_range_apply_button')
    pag.moveTo((x + x_offset), (y + y_offset), duration=rand_move_timeout())
    pag.click()    
    
    """
    START BACKTEST
    """
           
    #start backtest
    
    pag.scroll(-500+y_offset)
    print("Start backtest.. ")   
    x, y = get_x_and_y_position(data, 'start_backtest')    
    pag.moveTo((x + x_offset), (y + y_offset), duration=rand_move_timeout())
    pag.click()
    
    sleep_with_print(backtest_delay + 10) #set to 60 sec for 3 day demo backtest
    print("delay: {}".format((backtest_delay + 10)))
    
    
    """
    SAVE RESULTS SS            
    """
    
    #scroll down to view results, take screenshot as backup    
    print("Save results screenshot.. ")        
    pag.scroll(-375)    
    time.sleep(rand_move_timeout())
       
    enable_multi_screen_grabs()
    pag.screenshot('backtest_results_test.png')
    
    
    """
    COPY RESULTS TO CLIPBOARD
    """    
    
    print("Copy results to clipboard")
    
    #move to results bottom right, drag to top left, right click to copy selected, copy to clipboard
    time.sleep(rand_move_timeout())    
    x, y = get_x_and_y_position(data, 'backtest_result_extraction_lower_right')       
    pag.moveTo((x + x_offset), (y + y_offset), duration=rand_move_timeout())   
    
    x, y = get_x_and_y_position(data, 'backtest_result_extraction_top_left') 
    pag.dragTo((x + x_offset), (y + y_offset), duration=rand_move_timeout())   
    
    pag.rightClick()
    pag.moveRel(30, 10, duration=rand_move_timeout())  
    
    pag.click()
    
      
    """
    SAVE FROM CLIPBOARD TO FILE
    """
    print("Save backtest results to file..")
    
    #save copied results from clipboard into a file
    results = pyperclip.paste() 
    
    f_name = '{}_tp{}_backtest_results.txt'.format(strategy_name, take_profit_limit_percent)
    full_path = settings.RESULTS_BASEPATH.joinpath(f_name)
    with open(full_path,'a') as g:
        g.write(results)

    """
    TRANSFER CLEANED UP VERSION TO OTHER FILE
    """
    print("Cleanup results file..")
    
    #post process results file
    cleaned_up_path = settings.RESULTS_BASEPATH.joinpath('{}_tp{}_backtest_results_clean.txt'.format(strategy_name, take_profit_limit_percent))
    with open(full_path) as infile, open(cleaned_up_path, 'a') as outfile:
        outfile.write('Coin dropdown nr: {}\n'.format(currency_dropdown_item_nr))
        outfile.write('Take profit limit: {}\n'.format(take_profit_limit_percent))
        for line in infile:
            if not line.strip(): continue  # skip the empty line
            outfile.write(line)  # non-empty line. Write it to output
        outfile.write('\n-----\n')
    
    """
    REMOVE INITIAL DUMP FILE
    """
    
    #full_path = '{}_tp{}_backtest_results.txt'.format(strategy_name, take_profit_limit_percent)
    os.remove(full_path)
    
    """
    RETURN TO BEGINNING
    """
    print("Test finished. Returning to start..")
    
    #return to dashboard start position
    x, y = get_x_and_y_position(data, 'dashboard')   
    pag.moveTo((x + x_offset), (y + y_offset), duration=0.86)
    pag.click()
    
    time.sleep(2.13)
    
    #done, go again..

def print_start(strategy, period, tp):
    time.sleep(1)
    print(3)
    time.sleep(1)
    print(2)
    time.sleep(1)
    print(1)
    print('-'*20)
    print("Starting run: {}".format(strategy))
    print("Backtest period: {}".format(period))
    print("TP percentage: {}".format(tp))
    print('-'*20)

def main():
    
    global speaker
    speaker = Voice()
    
    strategy_name = 'Profit Scalping PREMIUM - DEX'    
    backtest_period = '06/1/2021 12:00 AM - 06/3/2021 11:59 PM'
    take_profit_percentages = [0.4, 0.5, 0.6]
    coin_positions = [3,4,5,6]
    
    # RunConfig(strategy_name, backtest_period = '06/1/2021 12:00 AM - 06/3/2021 11:59 PM', coin_positions, take_profit_percentages)
    
    speak_and_print("Backtest batckrunner started")
        
    for n, coin in enumerate(coin_positions):
        speak_and_print("Backtest batchrunner started testing coin number: {}".format(coin))

        for tp in take_profit_percentages:
            run_backtest(coin, strategy_name, backtest_period, tp)
        print('Progress >|'+('|'*n)+ ('-'*(len(coin_positions)-n))+'<')
    
  
    speak_and_print("Backtest batckrunner has completed tests for {} with {} take profit setting.".format(strategy_name, len(take_profit_percentages)))

       
    
    #upload results to sheets
        
    mani = SheetManipulator()
    mani.connect_to_spreadsheet("KillerWhale Free version results")
    print("Connect to spreadsheet..")
    mani.add_worksheet(strategy_name)
    print("Adding worksheet..")
    #add headers
    mani.update_wks(strategy_name,'A1',[[backtest_period]])
    
    mani.update_wks(strategy_name, 'A2:K2',[["coin_dr_num",
                                             "tp_limit",
                                             "max_profit",
                                             "strategy",
                                             "SL",
                                             "TSL",
                                             "TSB",
                                             "Sells - success",
                                             "Sells - loss",
                                             "avg hold time",
                                             "date found"]])
    output_file = '{}_tp{}_backtest_results_clean.txt'.format(strategy_name, take_profit_percentage)
    
    #add results
    with open(output_file) as f:
        rundata = []
        for line in f:
            
            if ":" in line:
                segments = line.split(':')
                rundata.append(segments[1])
            elif "-----" in line:
                mani.update_wks(strategy_name,'A{}:K{}'.format(mani.next_empty_row(strategy_name), mani.next_empty_row(strategy_name)),[rundata])
                rundata = []
            else:
                continue
            
    speak_and_print("Sheet manipulator uploaded backtest results to google sheets.")    
    speak_and_print("Shall we run more tests, Mister Underhill?")
             
   # mani.update_wks(strategy_name,[rundata])
    #for i in range(2,20):
        #run_backtest(i, 'Scalp Star - Free', '05/20/2021 12:00 AM - 05/31/2021 11:59 PM', 0.65)

if __name__ == '__main__':
    main()
