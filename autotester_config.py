

class RunConfig():
    
    def __init__(self, strategy_name, backtest_period, coin_positions, take_profit_percentages):
        
        """
        init with minimum:
        strategy_name - string
        coin_positions - list of positions
        backtest_period - string
        take_profit_percentages - list of floats
        #todo add stoploss, TSL , TSB, SELL based on strat etc.        
        """
        self.strategy_name = strategy_name
        self.coin_positions = coin_positions
        self.backtest_period = backtest_period
        self.take_profit = take_profit_percentages
        
    
    def get_strat(self):
        return self.strategy_name
        
    def get_coin_positions(self):
        return self.coin_positions
    
    def get_backtest_period(self):
        return self.backtest_period
    
    def get_take_profit(self):
        return self.take_profit
        
        